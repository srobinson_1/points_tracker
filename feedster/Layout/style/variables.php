<?php
	header("Content-type: text/css; charset: UTF-8"); 
	
	$base = "#ffffff";
	$main_accent = "#d95300";
	$pagewidth = "1000";
	$sidebarwidth = "150";
?>

  @base: <?php echo $base; ?>;
  @main_accent: <?php echo $main_accent; ?>;
  @pagewidth: <?PHP echo $pagewidth . "px"; ?>;
  @sidebarwidth: <?PHP echo $sidebarwidth . "px"; ?>;

  @import "style.less";