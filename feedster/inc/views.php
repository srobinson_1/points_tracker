<?PHP
function formLogin(){
	echo"<p><form method=\"POST\">
		"; language("page_string_username"); echo": <input type=\"text\" name=\"username\"><br>
		"; language("page_string_password"); echo": <input type=\"password\" name=\"password\"><br>
		"; language("page_string_rememberme"); echo" <input type=\"checkbox\" name=\"rememberme\"><br>
		<input type=\"submit\" name=\"submit\" value=\""; language("page_button_login"); echo"\">
		</form></p>";
}

function formRegister(){
	echo"<p><form method=\"POST\">
		"; language("page_string_desiredusername"); echo":<input type=\"text\" name=\"username\"><br>
		"; language("page_string_password"); echo": <input type=\"password\" name=\"pass\"><br>
		"; language("page_string_confirm_password"); echo": <input type=\"password\" name=\"cpass\"><br>
		"; language("page_string_email"); echo": <input type=\"text\" name=\"email\"><br>
		<input type=\"submit\" name=\"submit\" value=\""; language("page_button_register"); echo"\">
		</form></p>";
}

function formSystemSettings($pageforwardtime, $rememberme,  $login, $registration){
	echo"<p><form method=\"POST\">
		<fieldset>
		<legend>"; language("page_string_global_settings"); echo"</legend>
		"; language("page_string_forward_time"); echo": <input type=\"text\" name=\"pagefowardtime\" value=\"" . $pageforwardtime . "\"> "; language("page_string_in_seconds"); echo"<br>
		"; language("page_string_rememberme"); echo": <input type=\"text\" name=\"rememberme\" value=\"" . $rememberme . "\"> "; language("page_string_in_days"); echo"
		</fieldset>
		<fieldset>
		<legend>"; language("page_string_site_features"); echo"</legend>
		"; language("link_login"); echo": <input type=\"radio\" name=\"logintoggle\" value=\"1\" "; if($login == True) { echo"CHECKED"; } echo">"; language("page_string_on"); echo"  &nbsp;	<input type=\"radio\" name=\"logintoggle\" value=\"0\" "; if($login == False) { echo"CHECKED"; } echo">"; language("page_string_off"); echo"<br>
		"; language("link_register"); echo": <input type=\"radio\" name=\"registertoggle\" value=\"1\" "; if($registration == True) { echo"CHECKED"; } echo">"; language("page_string_on"); echo" &nbsp; <input type=\"radio\" name=\"registertoggle\" value=\"0\" "; if($registration == False) { echo"CHECKED"; } echo">"; language("page_string_off"); echo"
		</fieldset>
		<input type=\"submit\" name=\"submit\" value=\""; language("page_button_submit"); echo"\">
		</form></p>";
}

function formLanguage($sysname, $string, $id){
	echo"<p><form method=\"POST\">
	"; language("page_string_sysname", false); echo": <input type=\"text\" name=\"sysname\" value=\"" . $sysname . "\" /><br>
	"; language("page_string_string", false); echo": <input type=\"text\" name=\"string\" value=\"" . $string . "\" /><br>
	<input type=\"submit\" name=\"submit\" value=\""; language("page_button_submit"); echo"\" />
	</form></p>";
	if(!empty($id)){
		echo"<p><a href=\"?page=admin&section=language&subsection=delete&string=" . $id . "\">"; language("page_string_delete", false); echo"</a></p>";
	}
}

function formAddons($name, $description, $state, $navigation, $navname, $functionsfile, $mainfile, $addonid){
	echo"<p><form method=\"POST\">
	"; language("page_string_name"); echo": <input type=\"text\" name=\"name\" value=\"" . $name . "\" /><br>
	"; language("page_string_description"); echo": <input type=\"text\" name=\"description\" value=\"" . $description . "\" /><br>
	"; language("page_string_state"); echo": <input type=\"radio\" name=\"state\" value=\"1\" "; if($state == True) { echo"CHECKED"; } echo">"; language("page_string_on"); echo"  &nbsp;	<input type=\"radio\" name=\"state\" value=\"0\" "; if($state == False) { echo"CHECKED"; } echo">"; language("page_string_off"); echo"<br>
	"; language("page_string_navigation"); echo":  <input type=\"radio\" name=\"navigation\" value=\"1\" "; if($navigation == True) { echo"CHECKED"; } echo">"; language("page_string_on"); echo"  &nbsp;	<input type=\"radio\" name=\"navigation\" value=\"0\" "; if($navigation == False) { echo"CHECKED"; } echo">"; language("page_string_off"); echo"<br>
	"; language("page_string_navigation_text"); echo": <input type=\"text\" name=\"navname\" value=\"" . $navname . "\" /><br>
	"; language("page_string_functions_file"); echo": <input type=\"text\" name=\"functionsfile\" value=\"" . $functionsfile . "\" /><br>
	"; language("page_string_main_file"); echo": <input type=\"text\" name=\"mainfile\" value=\"" . $mainfile . "\" /><br>
	<input type=\"submit\" name=\"submit\" value=\""; language("page_button_submit"); echo"\">
	</form></p>";
	if(!empty($addonid)){
		echo"<p><a href=\"?page=admin&section=addons&subsection=delete&addonid=" . $addonid . "\">"; language("page_string_delete", false); echo"</a></p>";
	}
}

function MenuAdminMain(){
	echo"- <a href=\"?page=admin&amp;section=system\">"; language("link_admin_settings"); echo"</a><br>";
	echo"- <a href=\"?page=admin&amp;section=language\">"; language("link_admin_language"); echo"</a><br>";
	echo"- <a href=\"?page=admin&amp;section=addons\">"; language("link_admin_addons"); echo"</a><br>";
	echo"- <a href=\"?page=admin&amp;section=cleandb\">"; language("link_admin_cleandb"); echo"</a><br>";
}

function listLanguage(){
	echo"<a href=\"?page=admin&section=language&subsection=edit\">"; language("page_string_newstring", false); echo"</a><br>
	<table><thead><tr><td>"; language("page_string_name", false); echo"</td><td>"; language("page_string_description", false); echo"</td><td>&nbsp;</td></tr></thead><tbody>";
	$getlangauge = mysql_query("SELECT * FROM language ORDER BY sysname ASC");
	while($fetch = mysql_fetch_array($getlangauge)){
		echo"<tr><td>" . $fetch['sysname'] . "</td><td>" . $fetch['value'] . "</td><td><a href=\"?page=admin&section=language&subsection=edit&string=" . $fetch['id'] . "\">"; language("page_string_edit", false); echo"</a></td></tr>";
	}
	echo"</tbody></table>";
}

function listAddons(){
	echo"<a href=\"?page=admin&section=addons&subsection=edit\">"; language("page_string_newaddon", false); echo"</a><br>
	<table><thead><tr><td>"; language("page_string_name", false); echo"</td><td>"; language("page_string_description", false); echo"</td><td>"; language("page_string_state", false); echo"</td><td>&nbsp;</td></tr></thead><tbody>";
	$getaddon = mysql_query("SELECT * FROM addons ORDER BY id ASC");
	while($fetch = mysql_fetch_array($getaddon)){
		echo"<tr><td>" . $fetch['name'] . "</td><td>" . nl2br($fetch['description']) . "</td><td>" . $fetch['state'] . "</td><td><a href=\"?page=admin&section=addons&subsection=edit&addonid=" . $fetch['id'] . "\">"; language("page_string_edit", false); echo"</a></td></tr>";
	}
	echo"</tbody></table>";
}

function genericPageHeader($text){
	echo"<h2>" . language($text, false, true) . "</h2>";
}
?>