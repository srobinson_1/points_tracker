-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 11, 2013 at 05:24 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bms3`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE IF NOT EXISTS `addons` (
  `id` int(4) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `state` tinyint(1) NOT NULL,
  `navigation` tinyint(1) NOT NULL,
  `navname` varchar(100) NOT NULL,
  `functionsfile` varchar(100) NOT NULL,
  `mainfile` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`id`, `name`, `description`, `state`, `navigation`, `navname`, `functionsfile`, `mainfile`) VALUES
(1, 'Notification Control', 'Pass notifications to a single user or groups of users.', 0, 1, 'Notifications', 'addon_notifications_func', 'addon_notifications'),
(2, 'Project Manager', 'Manage projects of all sizes with one or multiple developers.', 1, 1, 'Project Management', 'addon_projectmanager_func', 'addon_projectmanager');

-- --------------------------------------------------------

--
-- Table structure for table `addon_notifications`
--

CREATE TABLE IF NOT EXISTS `addon_notifications` (
  `id` int(11) NOT NULL auto_increment,
  `forusr` int(11) NOT NULL,
  `related_addon` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `text` text NOT NULL,
  `link` varchar(300) NOT NULL,
  `read` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `addon_notifications`
--

INSERT INTO `addon_notifications` (`id`, `forusr`, `related_addon`, `title`, `text`, `link`, `read`) VALUES
(5, 1, 1, '1', '1', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_projmgr_developers`
--

CREATE TABLE IF NOT EXISTS `addon_projmgr_developers` (
  `id` int(11) NOT NULL auto_increment,
  `usrid` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `addon_projmgr_projects`
--

CREATE TABLE IF NOT EXISTS `addon_projmgr_projects` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(4) NOT NULL auto_increment,
  `sysname` varchar(300) NOT NULL,
  `value` text NOT NULL,
  `lang` varchar(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `sysname`, `value`, `lang`) VALUES
(1, 'user_already_loggedin', 'You are already logged in.', 'GB'),
(2, 'user_not_loggedin', 'You are not logged in.', 'GB'),
(3, 'user_username_taken', 'The username you entered has already been taken.', 'GB'),
(4, 'user_password_mismatch', 'The passwords you entered do not match.', 'GB'),
(5, 'user_email_invalid', 'The email address you entered is not a valid email address.', 'GB'),
(6, 'error_empty_fields', 'You left one or more mandatory fields blank.', 'GB'),
(7, 'error_amend_to_continue', 'Please amend all issues in order to continue', 'GB'),
(8, 'user_registration_success', 'You have successfully registered.', 'GB'),
(9, 'user_username_no_exist', 'The username you entered is not on the system.', 'GB'),
(10, 'user_login_success', 'Login Successful', 'GB'),
(11, 'user_welcome', 'Welcome, ', 'GB'),
(12, 'error_unauthorised_access', 'You do not have permission to view this page.', 'GB'),
(13, 'user_has_loggedout', 'You have been logged out. Thank you.', 'GB'),
(14, 'link_home', 'Home', 'GB'),
(15, 'link_login', 'Login', 'GB'),
(16, 'link_register', 'Register', 'GB'),
(17, 'link_logout', 'Logout', 'GB'),
(18, 'page_redirection', 'You will be redirected.', 'GB'),
(19, 'link_messages', 'Private Messages', 'GB'),
(20, 'error_feature_disabled', 'Sorry, this feature is currently unavailable', 'GB'),
(21, 'link_admin_settings', 'System Settings', 'GB'),
(22, 'error_pgfwdtime_not_numeric', 'Page Forward Time must be a numerical value', 'GB'),
(23, 'page_update_successful', 'The Changes were made successfully.', 'GB'),
(24, 'link_admin', 'Admin Panel', 'GB'),
(25, 'error_locked_admin_only', 'This feature is currently locked to Administrator use only. ', 'GB'),
(26, 'link_adminbypass', 'Administrator Bypass', 'GB'),
(27, 'user_password_incorrect', 'The password you entered was incorrect.', 'GB'),
(28, 'link_admin_language', 'Language Control', 'GB'),
(32, 'link_admin_addons', 'Addon Control', 'GB'),
(34, 'page_delete_confirmation', 'Are you sure you want to delete?', 'GB'),
(35, 'page_yes', 'Yes', 'GB'),
(36, 'page_no', 'No', 'GB'),
(39, 'page_delete_successful', 'Record deleted successfully.', 'GB'),
(41, 'page_string_name', 'Name', 'GB'),
(42, 'page_string_description', 'Description', 'GB'),
(43, 'page_string_state', 'State', 'GB'),
(44, 'page_string_delete', 'Delete', 'GB'),
(45, 'page_string_newstring', 'New String', 'GB'),
(46, 'page_string_edit', 'Edit', 'GB'),
(47, 'page_string_sysname', 'System Name', 'GB'),
(48, 'page_string_string', 'String', 'GB'),
(49, 'page_button_submit', 'Submit', 'GB'),
(50, 'page_button_login', 'Login', 'GB'),
(51, 'page_button_register', 'Register', 'GB'),
(52, 'page_string_username', 'Username', 'GB'),
(53, 'page_string_password', 'Password', 'GB'),
(54, 'page_string_desiredusername', 'Desired Username', 'GB'),
(55, 'page_string_confirm_password', 'Confirm Password', 'GB'),
(56, 'page_string_email', 'Email', 'GB'),
(57, 'page_string_forward_time', 'Page forward time', 'GB'),
(58, 'page_string_global_settings', 'Global Settings', 'GB'),
(59, 'page_string_site_features', 'Site Features', 'GB'),
(60, 'page_string_in_seconds', '(in seconds)', 'GB'),
(61, 'page_string_on', 'On', 'GB'),
(62, 'page_string_off', 'Off', 'GB'),
(65, 'page_string_newaddon', 'New Addon', 'GB'),
(66, 'page_string_navigation', 'Navigation', 'GB'),
(67, 'page_string_navigation_text', 'Navigation Text', 'GB'),
(68, 'page_string_functions_file', 'Functions File', 'GB'),
(69, 'page_string_main_file', 'Main File', 'GB'),
(70, 'link_admin_cleandb', 'Clean the Database', 'GB'),
(72, 'page_admin_purge_sessions', 'Purged old sessions', 'GB'),
(73, 'page_string_rememberme', 'Remember Me', 'GB'),
(74, 'page_string_in_days', '(in days)', 'GB'),
(75, 'error_rememberme_not_longerthanone', 'Remember Me must be greater than 1', 'GB'),
(76, 'notifs_page_title', 'Notifications', 'GB'),
(77, 'notifs_page_none', 'There are no notifications to display', 'GB');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(9) NOT NULL auto_increment,
  `uid` int(9) NOT NULL,
  `tag` int(9) NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `uid`, `tag`, `expires`) VALUES
(1, 1, 888845825, '2012-12-21 15:09:14'),
(2, 1, 637121582, '2013-01-03 08:49:53'),
(3, 1, 690350341, '2013-01-10 09:23:00'),
(4, 1, 648355102, '2013-01-16 17:02:09'),
(5, 1, 560601806, '2013-01-16 17:03:03'),
(6, 1, 870004272, '2013-01-16 17:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE IF NOT EXISTS `system` (
  `id` int(4) NOT NULL auto_increment,
  `login` tinyint(1) NOT NULL default '1',
  `registration` tinyint(1) NOT NULL default '1',
  `pageforwardtime` int(4) NOT NULL default '0',
  `rememberme` int(9) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`id`, `login`, `registration`, `pageforwardtime`, `rememberme`) VALUES
(1, 1, 1, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(4) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `rank` int(4) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `rank`) VALUES
(1, 'MatthewG', '83372b4f65991f38b0cfe9150ca6a60e', 'M', 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
