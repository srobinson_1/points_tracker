$(document).ready(function(){
	//alert('test');
});

function update_table(row_id,status){
	var row_colors = new Array();
	row_colors[1] = "ns";
	row_colors[2] = "oh";
	row_colors[3] = "c";
	row_colors[4] = "crt";
	row_colors[5] = "wc";
	row_colors[6] = "ip";
	row_colors[7] = "w";
	row_colors[8] = "ca";
	//alert('rowid_' + row_id);
	document.getElementById('rowid_' + row_id).setAttribute("class", row_colors[status]);
}

function update_data(ticket_id, value, name1 ){
	var start_date = document.getElementById(ticket_id + '_start_date').value;
	var ticket = document.getElementById(ticket_id + '_ticket').value;
	var name = document.getElementById(ticket_id + '_name').value;
	var status = document.getElementById(ticket_id + '_status');
	var selected_status = status.options[status.selectedIndex].value;
	var type = document.getElementById(ticket_id + '_type');
	var selected_type = type.options[type.selectedIndex].value;
	var completed = document.getElementById(ticket_id + '_completed').value;
	var points = document.getElementById(ticket_id + '_points').value;
	var assign = document.getElementById(ticket_id + '_assigned').value;
	
	var post_data = "id=" + ticket_id 
		+ "&ticket=" + ticket 
		+ "&name=" + name
		+ "&status=" + selected_status
		+ "&type=" + selected_type
		+ "&points=" + points
		+ "&start_date=" + start_date
		+ "&completed=" + completed
		+ "&assigned=" + assign;
		//alert(post_data);
	ajax_call('http://192.168.1.114/feedster/page/work_func.php','POST', post_data);
	update_table(ticket_id, selected_status);	
}

function ajax_call( url, type, data ){
	$.ajax({
  	type: type,
  	url: url,
  	data: data,
  	success: function(data){
		//alert(data);
  	},
  	dataType: 'text'
	});
}

