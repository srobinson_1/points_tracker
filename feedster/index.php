<?PHP
if(!file_exists('inc/config.php') && $page != 'install'){
	header("Location: install.php");
}else{
	include"inc/config.php";
	include"inc/nav.php";
	include"inc/Chart.php";
?>
<!DOCTYPE html>
	<html>
		<head>
			<title>Integrations points tracker</title>
			<link rel='stylesheet' type='text/css' href='css/style.css' />
			<style>
				.chart div {
				  font: 10px sans-serif;
				  background-color: steelblue;
				  text-align: right;
				  padding: 3px;
				  margin: 1px;
				  color: white;
				}
				
				.axis {
				  font: 10px sans-serif;
				}

				.axis path,
				.axis line {
				  fill: none;
				  stroke: #000;
				  shape-rendering: crispEdges;
				}
			</style>
			<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
			<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
			<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
			<script>
			$(function() {
			$( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
			});
			</script>
		</head>
		<body>
		<div id='container' id='container'>
<?PHP
	if(isset($_GET['page'])){
		include"page/" . $_GET['page'] . ".php";
	}else{
		include"page/home.php";
	}
}
?>
		</div>
		
		<script type='text/javascript' src='js/feedster.js'></script>
		</body>
	</html>