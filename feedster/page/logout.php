<?PHP
if($logged['username']){
	setcookie("id", "0",time()+(60*60*24), "", ""); 
	setcookie("tag", "loggedout",time()+(60*60*24), "", "");
	language("user_has_loggedout", true);
	language("page_redirection", true);
	movePage("?page=home", $setting['pageforwardtime']);
}else{
	language("error_unauthorised_access", true);
}
?>