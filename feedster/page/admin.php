<?PHP
if($logged['username'] && $logged['rank'] >= 6){
	$section = "default";
	if(!empty($_GET['section'])){
		$section = $_GET['section'];
	}
	switch($section){
		default:
		case 'default':
			menuAdminMain();
		break;
		case 'system':
			if(!isset($_POST['submit'])){ 
				$setting = mysql_query("SELECT * FROM system WHERE id = '1'");
				$setting = mysql_fetch_array($setting);
				formSystemSettings($setting['pageforwardtime'], $setting['rememberme'], $setting['login'], $setting['registration']);
			}else{
				$pagefowardtime = $_POST['pagefowardtime'];
				$rememberme = $_POST['rememberme'];
				$logintoggle = $_POST['logintoggle'];
				$resistertoggle = $_POST['registertoggle'];
				
				$errors = 0;
				if(!is_numeric($pagefowardtime)){
					language("error_pgfwdtime_not_numeric", true);
					$errors++;
				}
				if($rememberme <= 1){
					language("error_rememberme_not_longerthanone", true);
					$errors++;
				}
				
				if($errors == 0){
					$update = mysql_query("UPDATE system SET pageforwardtime = '" . $pagefowardtime . "', rememberme = '" . $rememberme . "', login = '" . $logintoggle . "', registration = '" . $resistertoggle . "' WHERE id = '1'");
					language("page_update_successful", true);
					movePage("?page=admin", $setting['pageforwardtime']);				
				}
			}
		break;
		case 'language':
			$subsection = "default";
			if(!empty($_GET['subsection'])){
				$subsection = $_GET['subsection'];
			}
			$string = NULL;
			if(!empty($_GET['string'])){
				$string = $_GET['string'];
			}
			switch($subsection){
				default:
				case 'default':
					listLanguage();
				break;
				case 'edit':
					if(!isset($_POST['submit'])){
						$sysname = NULL;
						$txtstring = NULL;
						if(!empty($string)){
							$query = mysql_query("SELECT * FROM language WHERE id = '" . $string . "'");
							$fetch = mysql_fetch_array($query);
							$sysname = $fetch['sysname'];
							$txtstring = $fetch['value'];
						}
						formLanguage($sysname, $txtstring, $string);
					}else{
						$sysname = $_POST['sysname'];
						$txtstring = $_POST['string'];
						
						if(empty($sysname) || empty($txtstring)) {
							language("error_empty_fields", true);
						}else{
							if(empty($string)){
								$create = mysql_query("INSERT INTO language (`sysname`, `value`, `lang`) VALUES ('" . $sysname . "', '" . $txtstring . "', 'GB')");
							}else{
								$update = mysql_query("UPDATE language SET sysname = '" . $sysname . "', value = '" . $txtstring . "' WHERE id = '" . $string . "'");
							}
							language("page_redirection", true);
							movePage("?page=admin&section=language", $setting['pageforwardtime']);
						}
					}
				break;
				case 'delete':
					if(empty($string)){
						movePage("?page=admin&section=language", $setting['pageforwardtime']);
					}else{
						$delconf = 0;
						if(isset($_GET['delconf'])){
							$delconf = $_GET['delconf'];
						}
						if($delconf != 1){
							language("page_delete_confirmation", true);
							echo"<a href=\"?page=admin&section=language&subsection=delete&string=" . $string . "&delconf=1\">"; language("page_yes", false); echo"</a> &nbsp; <a href=\"?page=admin&section=language&subsection=edit&string=" . $string . "\">"; language("page_no", false); echo"</a>";
						}else{
							$delete = mysql_query("DELETE FROM language WHERE id = '" . $string . "'");
							language("page_delete_successful", true);
							movePage("?page=admin&section=language", $setting['pageforwardtime']);
						}
					}
				break;
			}
		break;
		case 'addons':
			$subsection = "default";
			if(!empty($_GET['subsection'])){
				$subsection = $_GET['subsection'];
			}
			$addonid = NULL;
			if(!empty($_GET['addonid'])){
				$addonid = $_GET['addonid'];
			}
			switch($subsection){
				default:
				case 'default':
					listAddons();
				break;
				case 'edit':
					if(!isset($_POST['submit'])){
						$name = NULL;
						$description = NULL;
						$state = 0;
						$navigation = 0;
						$navname = NULL;
						$functionsfile = NULL;
						$mainfile = NULL;
						if(!empty($addonid)){
							$query = mysql_query("SELECT * FROM addons WHERE id = '" . $addonid . "'");
							$fetch = mysql_fetch_array($query);
							$name = $fetch['name'];
							$description = $fetch['description'];
							$state = $fetch['state'];
							$navigation = $fetch['navigation'];
							$navname = $fetch['navname'];
							$functionsfile = $fetch['functionsfile'];
							$mainfile = $fetch['mainfile'];
						}
						formAddons($name, $description, $state, $navigation, $navname, $functionsfile, $mainfile, $addonid);
					}else{
						$name = $_POST['name'];
						$description = $_POST['description'];
						$state = $_POST['state'];
						$navigation = $_POST['navigation'];
						$navname = $_POST['navname'];
						$functionsfile = $_POST['functionsfile'];
						$mainfile = $_POST['mainfile'];
						
						if(empty($name) || empty($description) || empty($functionsfile) || empty($mainfile)) {
							language("error_empty_fields", true);
						}else{
							if(empty($addonid)){
								$create = mysql_query("INSERT INTO addons (`name`, `description`, `state`, `navigation`, `navname`, `functionsfile`, `mainfile`) VALUES ('" . $name . "', '" . $description . "', '" . $state . "', '" . $navigation . "', '" . $navname . "', '" . $functionsfile . "', '" . $mainfile . "')");
							}else{
								$update = mysql_query("UPDATE addons SET name = '" . $name . "', description = '" . $description . "', state = '" . $state . "', navigation = '" . $navigation . "', navname = '" . $navname . "', functionsfile = '" . $functionsfile . "', mainfile = '" . $mainfile . "' WHERE id = '" . $addonid . "'");
							}
							language("page_redirection", true);
							movePage("?page=admin&section=addons", $setting['pageforwardtime']);
						}
					}
				break;
				case 'delete':
					if(empty($addonid)){
						movePage("?page=admin&section=addons", $setting['pageforwardtime']);
					}else{
						$delconf = 0;
						if(isset($_GET['delconf'])){
							$delconf = $_GET['delconf'];
						}
						if($delconf != 1){
							language("page_delete_confirmation", true);
							echo"<a href=\"?page=admin&section=addons&subsection=delete&addonid=" . $addonid . "&delconf=1\">"; language("page_yes", false); echo"</a> &nbsp; <a href=\"?page=admin&section=addons&subsection=edit&addonid=" . $addonid . "\">"; language("page_no", false); echo"</a>";
						}else{
							$delete = mysql_query("DELETE FROM addons WHERE id = '" . $addonid . "'");
							language("page_delete_successful", true);
							movePage("?page=admin&section=addons", $setting['pageforwardtime']);
						}
					}
				break;
			}
		break;
		case 'cleandb':
			$deloldsessions = mysql_query("DELETE FROM session WHERE expires < '" . $timenow . "'");
			language("page_admin_purge_sessions", true);
			language("page_delete_successful", true);
			movePage("?page=admin", 2);
		break;
	}
}else{
	language("error_unauthorised_access", true);
	echo"<a href=\"?page=login&amp;adminbypass\">"; language("link_adminbypass"); echo"</a>";
}
?>